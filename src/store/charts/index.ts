import { Module } from 'vuex'
import { StateInterface } from '../index'
import state, { ChartsStateInterface } from './state'
// import actions from './actions'
// import getters from './getters'
// import mutations from './mutations'

const chartsModule: Module<ChartsStateInterface, StateInterface> = {
  namespaced: true,
  // actions,
  // getters,
  // mutations,
  state
}

export default chartsModule
