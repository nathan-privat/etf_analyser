export interface CandleStickSeries {
  data: (number | number[])[]
  minTimeStamp: number
  maxTimeStamp: number
}

export interface ChartsStateInterface {
  currentSeries: CandleStickSeries
}

const state: ChartsStateInterface = {
  currentSeries: {
    data: [],
    minTimeStamp: 0,
    maxTimeStamp: 0,
  }
}

export default state
