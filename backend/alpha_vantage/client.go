package alpha_vantage

import (
	"etf_analyzer_backend/candle_series"
	"etf_analyzer_backend/time_series"
  "fmt"
  "net/http"
  "net/url"
  "time"
)

const (
  AlphaVantageHost = "www.alphavantage.com"
)

type AvClient struct {
  client *http.Client
  apiKey string
}

// NewClient creates a new connection at the default Alpha Vantage host
func NewClient(apiKey string) *AvClient {
  client := &http.Client{
    Timeout: 30 * time.Second,
  }
  return &AvClient{
    client: client,
    apiKey: apiKey,
  }
}

func (c *AvClient) Request(endpoint *url.URL) (*http.Response, error) {
  endpoint.Scheme = "https"
  endpoint.Host = AlphaVantageHost
  return c.client.Get(endpoint.String())
}

// StockTimeSeriesIntraday queries a stock symbols statistics throughout the day.
// Data is returned from past to present.
func (c *AvClient) StockTimeSeriesIntraday(symbol string, interval int, year int, month int) ([]*candle_series.TimeSeriesValue, error) {
  endpoint := c.buildRequestPath(map[string]string{
    "function": "TIME_SERIES_INTRADAY_EXTENDED",
    "interval": fmt.Sprintf("%din", interval),
    "slice": fmt.Sprintf("year%dmonth%d", year, month),
    "symbol":   symbol,
  })
  response, err := c.Request(endpoint)
  if err != nil {
    return nil, err
  }
  defer response.Body.Close()
  return time_series.parseTimeSeriesCsvData(response.Body)
}

// buildRequestPath builds an endpoint URL with the given query parameters
func (c *AvClient) buildRequestPath(params map[string]string) *url.URL {
  // build our URL
  endpoint := &url.URL{}
  endpoint.Path = "query"

  // base parameters
  query := endpoint.Query()
  query.Set("apikey", c.apiKey)
  query.Set("datatype", "csv")
  query.Set("outputsize", "compact")

  // additional parameters
  for key, value := range params {
    query.Set(key, value)
  }

  endpoint.RawQuery = query.Encode()

  return endpoint
}
