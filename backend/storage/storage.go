package storage

import (
	"errors"
	"etf_analyzer_backend/candle_series"
	"etf_analyzer_backend/time_series"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"regexp"
	"strconv"
	"sync"
	"time"
)

type TimeSeriesResolution string

type Storage struct {
	mux          sync.RWMutex
	query        chan DownloadQuery
	DataBasePath string
}

type TimeSeriesDataSetName struct {
  Symbol string
  Interval int
  Year int
  Month time.Month
}

type TimeSeriesDataSubsetName struct {
  DataSetName TimeSeriesDataSetName
  SubTimeFrame time_series.TimeFrame
}

func (subset* TimeSeriesDataSubsetName) IsComplete() bool {
  return subset.SubTimeFrame.IsFullMonth()
}

func (s* Storage) Directory(data* TimeSeriesDataSetName) string {
	return path.Join(s.DataBasePath, data.Symbol, strconv.Itoa(data.TimeFrame().Start.Year()), fmt.Sprintf("%02d", int(data.TimeFrame().Start.Month())), strconv.Itoa(int(data.Interval)))
}

func (s* Storage) Path(data* TimeSeriesDataSetName) string {
  return path.Join(s.Directory(data), "all.dat")
}

func (s* Storage) SubsetPath(data* TimeSeriesDataSubsetName) string {
  if data.IsComplete() {
    return s.Path(&data.DataSetName)
  }
  // Not complete ...
  if data.SubTimeFrame.StartsWithMonth() {
    return path.Join(
      s.Directory(&data.DataSetName),
      fmt.Sprintf("till_%s.dat", data.SubTimeFrame.End.Format("2006-01-02T15-04-05")),
    )
  }
  if data.SubTimeFrame.EndsWithMonth() {
    return path.Join(
      s.Directory(&data.DataSetName),
      fmt.Sprintf("from_%s.dat", data.SubTimeFrame.Start.Format("2006-01-02T15-04-05")),
    )
  }
  return path.Join(
    s.Directory(&data.DataSetName),
    fmt.Sprintf(
      "between_%s_%s.dat",
      data.SubTimeFrame.Start.Format("2006-01-02T15-04-05"),
      data.SubTimeFrame.End.Format("2006-01-02T15-04-05"),
    ),
  )
}

func (dn *TimeSeriesDataSetName) TimeFrame() time_series.TimeFrame {
	startTime := time.Date(dn.Year, dn.Month, 1, 0, 0, 0, 0, time.UTC)
	return time_series.TimeFrame{
		Start: startTime,
		End:   startTime.AddDate(0, 1, 0),
	}
}

func timeRoundedToDay(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

func timeRoundedToMonth(t time.Time) time.Time {
	year, month, _ := t.Date()
	return time.Date(year, time.Month(month), 1, 0, 0, 0, 0, time.UTC)
}

func timeRoundedToYear(t time.Time) time.Time {
	year, _, _ := t.Date()
	return time.Date(year, time.January, 1, 0, 0, 0, 0, time.UTC)
}

// Returns a list of strings for the data symbol names for an intraday series
func getDays(start time.Time, end time.Time) []string {
	startDay := timeRoundedToDay(start)
	endDay := timeRoundedToDay(end)

	var result []string
	for currentDay := startDay; currentDay.Before(endDay) || currentDay.Equal(endDay); currentDay = currentDay.AddDate(0, 0, 1) {
		result = append(result, currentDay.Format("2006-01-02"))
	}
	return result
}

func getYears(start time.Time, end time.Time) []string {
	startYear := timeRoundedToYear(start)
	endYear := timeRoundedToYear(end)

	var result []string
	for currentYear := startYear; currentYear.Before(endYear) || currentYear.Equal(endYear); currentYear = currentYear.AddDate(1, 0, 0) {
		result = append(result, currentYear.Format("2006"))
	}
	return result
}

// fileOrDirExists checks if a file exists
func fileOrDirExists(filename string) bool {
	_, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return true
}

func (s *Storage) listAvailableDatasetFiles(name TimeSeriesDataSetName) (result []string, err error) {
	if !fileOrDirExists(s.Path(&name)) {
		return
	}

	contents, err := ioutil.ReadDir(s.Path(&name))
	if err != nil {
		return
	}
	for _, object := range contents {
		if object.IsDir() {
			continue
		}
		result = append(result, object.Name())
	}
	return
}

func getMonths(timeFrame time_series.TimeFrame) (result []time.Month) {
	// Find the first month start
	firstMonthStart := time.Date(timeFrame.Start.Year(), timeFrame.Start.Month(), 1, 0, 0, 0, 0, time.UTC)
	for iterator := firstMonthStart; iterator.Before(timeFrame.End); iterator = iterator.AddDate(0, 1, 0) {
		result = append(result, iterator.Month())
	}
	return
}

func getMonthlyTimeFrames(timeFrame time_series.TimeFrame) (result []time_series.TimeFrame) {
	// Find the first month start
	firstMonthStart := time.Date(timeFrame.Start.Year(), timeFrame.Start.Month(), 1, 0, 0, 0, 0, time.UTC)
	for iterator := firstMonthStart; iterator.Before(timeFrame.End); iterator = iterator.AddDate(0, 1, 0) {
		start := iterator
		if start.Before(timeFrame.Start) {
			start = timeFrame.Start
		}
		end := iterator.AddDate(0, 1, 0)
		if end.After(timeFrame.End) {
			end = timeFrame.End
		}
		result = append(result, time_series.TimeFrame{
			Start: start,
			End:   end,
		})
	}

	return result
}

func (s *Storage) listAvailableTimeFrames(name TimeSeriesDataSetName) (result []TimeSeriesDataSubsetName, err error) {
	s.mux.RLock()
	defer s.mux.RUnlock()

	// Time frames are based on files
	files, err := s.listAvailableDatasetFiles(name)
	if err != nil {
		return nil, err
	}
	for _, fileName := range files {
		// If all.dat ais available, we are done
		if fileName == "all.data" {
			result = []TimeSeriesDataSubsetName{{
				DataSetName:        name,
        SubTimeFrame:  name.TimeFrame(),
			}}
			return
		}
		// Add to the subsets - case Start till time
		if match, _ := regexp.MatchString("till_.*\\.dat", fileName); match {
      endTime, err := time.Parse("2006-01-02T15-04-05", fileName[5:len(fileName)-4])
      if err != nil {
        return nil, err
      }
      result = append(result, TimeSeriesDataSubsetName{
        DataSetName: name,
        SubTimeFrame: time_series.TimeFrame{
          Start: name.TimeFrame().Start,
          End:   endTime,
        },
      })
    }
		// Add to subset - case time till End Point
		if match, _ := regexp.MatchString("from_.*\\.dat", fileName); match {
			startTime, err := time.Parse("2006-01-02T15-04-05", fileName[5:len(fileName)-4])
			if err != nil {
				return nil, err
			}
			result = append(result, TimeSeriesDataSubsetName{
				DataSetName:         name,
        SubTimeFrame: time_series.TimeFrame{
					Start: startTime,
					End:   name.TimeFrame().End,
				},
			})
		}
	}
	return result, nil
}

// Test if a specific dataset is available
func (s *Storage) isDatasetAvailable(name TimeSeriesDataSetName) bool {
	s.mux.RLock()
	defer s.mux.RUnlock()

	return fileOrDirExists(path.Join(s.Path(&name), "all.dat"))
}

func (s *Storage) isDatasetTimeFrameAvailable(dn TimeSeriesDataSetName, timeFrame time_series.TimeFrame) bool {
	available, err := s.listAvailableTimeFrames(dn)
	if err != nil {
		return false
	}
	for _, subset := range available {
		if subset.SubTimeFrame.Contains(&timeFrame) {
			return true
		}
	}
	return false
}

// Test if a timeframe is available
func (s *Storage) IsAvailable(symbol string, timeFrame time_series.TimeFrame) bool {
	// Get the month time frames we need
	dataSetTimeFrames := getMonthlyTimeFrames(timeFrame)
	for _, tf := range dataSetTimeFrames {
		dn := TimeSeriesDataSetName{
			Symbol: symbol,
			Year:   tf.Start.Year(),
			Month:  tf.Start.Month(),
		}
		if !s.isDatasetTimeFrameAvailable(dn, tf) {
			return false
		}
	}
	return true
}

func TrimTimeSeries(ts []*candle_series.TimeSeriesValue, timeFrame time_series.TimeFrame) []*candle_series.TimeSeriesValue {
	timeFrameStartUnix := timeFrame.Start.Unix()
	timeFrameEndUnix := timeFrame.End.Unix()

	startIndex := 0
	endIndex := len(ts) - 1
	for startIndex = 0; ts[startIndex].Timestamp < timeFrameStartUnix; startIndex++ {
	}
	for endIndex = len(ts) - 1; ts[endIndex].Timestamp > timeFrameEndUnix; endIndex++ {
	}

	if startIndex == 0 && endIndex == len(ts)-1 {
		return ts
	}
	return ts[startIndex:endIndex]
}

func (s *Storage) loadTrimmedDataset(dn TimeSeriesDataSetName, timeFrame time_series.TimeFrame) (*candle_series.TimeSeries, error) {
	available, err := s.listAvailableTimeFrames(dn)
	if err != nil {
		return nil, err
	}
	for _, subset := range available {
		if subset.SubTimeFrame.Contains(&timeFrame) {
			// This is our timeframe
			ts, err := candle_series.LoadTimeSeriesProtoBuf(s.SubsetPath(&subset))
			if err != nil {
				return nil, err
			}
			return ts.Trim(&timeFrame), nil
		}
	}
	return nil, errors.New("no dataset found for given timeframe")
}

// Get all the missing time frames
func (s *Storage) GetMissingTimeFrames(symbol string, timeFrame time_series.TimeFrame) (result []time_series.TimeFrame, err error) {
	for _, tf := range getMonthlyTimeFrames(timeFrame) {
		var available []TimeSeriesDataSubsetName
		available, err = s.listAvailableTimeFrames(TimeSeriesDataSetName{
			Symbol: symbol,
			Year:   tf.Start.Year(),
			Month:  tf.Start.Month(),
		})
		if err != nil {
			return
		}
		newTimeFrames := []time_series.TimeFrame{tf}
		for _, a := range available {
			newTimeFrames = time_series.SubtractFromMany(newTimeFrames, &a.SubTimeFrame)
		}
		result = append(result, newTimeFrames...)
	}
	return
}

// Get a dataset for a specific time frame, we expect it to be available
func (s *Storage) GetTimeSeries(symbol string, timeFrame time_series.TimeFrame) (result *candle_series.TimeSeries, err error) {
	// Get the month time frames we need
	dataSetTimeFrames := getMonthlyTimeFrames(timeFrame)
	var values []*candle_series.TimeSeriesValue
	for _, tf := range dataSetTimeFrames {
		dn := TimeSeriesDataSetName{
			Symbol: symbol,
			Year:   tf.Start.Year(),
			Month:  tf.Start.Month(),
		}
		var subTimeFrame *candle_series.TimeSeries
		subTimeFrame, err = s.loadTrimmedDataset(dn, tf)
		if err != nil {
		  result = nil
			return
		}
    values = append(values, subTimeFrame.Values...)
	}
	result = &candle_series.TimeSeries{
	  Symbol: symbol,
	  Interval: 60,
	  TimeFrame: timeFrame,
	  Values: values,
  }
	return
}

func (s* Storage) StoreTimeSeries(timeSeries *candle_series.TimeSeries) (err error) {
  // Get the month time frames we need
  dataSetTimeFrames := getMonthlyTimeFrames(timeSeries.TimeFrame)
  for _, tf := range dataSetTimeFrames {
    // Is it a full month?
    if tf.IsFullMonth() {
      err = timeSeries.Trim(&tf).SaveTimeSeriesProtoBuf(
        s.Path(&TimeSeriesDataSetName{
          Symbol: timeSeries.Symbol,
          Year:   tf.Start.Year(),
          Month:  tf.Start.Month(),
        }))
    } else {
      // Its a subset!
      err = timeSeries.Trim(&tf).SaveTimeSeriesProtoBuf(
        s.SubsetPath(
          &TimeSeriesDataSubsetName{
            SubTimeFrame: tf,
            DataSetName: TimeSeriesDataSetName{
              Symbol: timeSeries.Symbol,
              Year:   tf.Start.Year(),
              Month:  tf.Start.Month(),
            },
          },
        ),
      )
    }
    if err != nil {
      return
    }
  }
  return
}
