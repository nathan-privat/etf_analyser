package storage

import (
  "reflect"
  "testing"
  "time"
)

func TestGetDays(t *testing.T) {
  days := getDays(
    time.Date(2020, 8, 1, 11, 10, 3,0, time.UTC),
    time.Date(2020, 8, 10, 11, 10, 3,0, time.UTC),
  )
  if len(days)  != 10 {
    t.Errorf("Expecting 10 days, got %v", days)
  }

  if !reflect.DeepEqual(days,
      []string{"2020-08-01", "2020-08-02", "2020-08-03", "2020-08-04", "2020-08-05", "2020-08-06", "2020-08-07", "2020-08-08", "2020-08-09", "2020-08-10"}) {
    t.Errorf("Expecting different days, got %v", days)
  }
}

func TestGetYears(t *testing.T) {
  years := getYears(
    time.Date(2020, 8, 1, 11, 10, 3,0, time.UTC),
    time.Date(2030, 8, 10, 11, 10, 3,0, time.UTC),
  )
  if len(years)  != 11 {
    t.Errorf("Expecting 10 years, got %v", years)
  }

  if !reflect.DeepEqual(years,
    []string{"2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030"}) {
    t.Errorf("Expecting different years, got %v", years)
  }
}

