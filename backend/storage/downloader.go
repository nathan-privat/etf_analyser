package storage

import (
  "context"
  "etf_analyzer_backend/candle_series"
  "etf_analyzer_backend/time_series"
  "fmt"
  "log"
)

import finnhub "github.com/Finnhub-Stock-API/finnhub-go"

type DownloadQuery struct {
	Symbol    string
	TimeFrame time_series.TimeFrame
}

func (s *Storage) QueryDownload(query DownloadQuery) {
	s.query <- query
}

func (s *Storage) StartDownloader(apiKey string) {
	s.query = make(chan DownloadQuery, 100)
	go func() {
		finnhubClient := finnhub.NewAPIClient(finnhub.NewConfiguration()).DefaultApi
		auth := context.WithValue(context.Background(), finnhub.ContextAPIKey, finnhub.APIKey{
			Key: apiKey, // Replace this
		})

		for;; {
      fmt.Printf("waiting for new query")
      query := <-s.query
      fmt.Printf("new query: %v\n", query)
      missingTimeFrames, err := s.GetMissingTimeFrames(query.Symbol, query.TimeFrame)
      if err != nil {
        log.Fatalf("error on GetMissingTimeFrames: %s", err.Error())
      }
      fmt.Printf("#missing time frames: %d\n", len(missingTimeFrames))
      for _, timeFrame := range missingTimeFrames {
        stockCandles, _, err := finnhubClient.StockCandles(
          auth,
          query.Symbol,
          "M",
          timeFrame.Start.Unix(),
          timeFrame.End.Unix(),
          nil)
        if err != nil {
          log.Fatalf("error on finnhubClient.StockCandles: %s", err.Error())
        }
        // Store data
        fmt.Println("storing time frame")
        data := make([]*candle_series.TimeSeriesValue, len(stockCandles.T))
        for i := 0; i < len(stockCandles.T); i++ {
          data[i].Timestamp = stockCandles.T[i]
          candle_series.SetOpenWithFloat(data[i], stockCandles.O[i])
          candle_series.SetHighWithFloat(data[i], stockCandles.H[i])
          candle_series.SetLowWithFloat(data[i], stockCandles.L[i])
          candle_series.SetCloseWithFloat(data[i], stockCandles.C[i])
          candle_series.SetVolumeWithFloat(data[i], stockCandles.V[i])
        }
        timeSeries := candle_series.TimeSeries{
          Symbol: query.Symbol,
          Interval: 60,
          TimeFrame: query.TimeFrame,
          Values: data,
        }
        s.StoreTimeSeries(&timeSeries)
      }
      fmt.Printf("done\n")
    }
	}()
}
