package time_series

import (
  "errors"
	"log"
	"time"
)

type TimeFrame struct {
	Start time.Time
	End   time.Time
}

func earliest(a time.Time, b time.Time) time.Time {
	if a.Before(b) {
		return a
	} else {
		return b
	}
}

func latest(a time.Time, b time.Time) time.Time {
	if a.After(b) {
		return a
	} else {
		return b
	}
}

// Returns if a time frame contains another one
func (tf *TimeFrame) Contains(other *TimeFrame) bool {
	if other.Start.Before(tf.Start) {
		return false
	}
	if other.End.After(tf.End) {
		return false
	}
	return true
}

func (tf *TimeFrame) Within(other *TimeFrame) bool {
  return !other.Start.Before(tf.Start) && !other.End.After(tf.End)
}

func (tf* TimeFrame) Intersection(other *TimeFrame) (result *TimeFrame) {
  result = &TimeFrame{
    latest(tf.Start, other.Start),
    earliest(tf.End, other.End),
  }
  if result.Start.After(result.End) {
    result = nil
    return
  }
  return
}

// Returns error if they do not overlap
func (tf *TimeFrame) UnionIfOverlap(other *TimeFrame) (*TimeFrame, error) {
	if tf.Start.Add(time.Duration(time.Minute)).Before(other.End) &&
		other.Start.Add(time.Duration(time.Minute)).Before(tf.End) {
		return nil, errors.New("time frames to not overlap")
	}
	// OK, they do overlap, so lets merge them
	return &TimeFrame{
    earliest(tf.Start, other.Start),
    latest(tf.End, other.End),
	}, nil
}

func (tf *TimeFrame) Subtract(other *TimeFrame) []TimeFrame {
	if other.End.Before(tf.Start) {
		// No overlap because the other is completely before us
		return []TimeFrame{*tf}
	}
	if tf.End.Before(other.Start) {
		// No overlap because the other is completely after us
		return []TimeFrame{*tf}
	}
	// OK, we have some sort of overlap!
	if !tf.Start.Before(other.Start) && !tf.End.After(other.Start) {
		// Completely overlap
		return []TimeFrame{}
	}
	if other.Start.After(tf.Start) && other.End.Before(tf.End) {
		// Other is inside us, its splitting us
		return []TimeFrame{
			{
				Start: tf.Start,
				End:   other.Start,
			},
			{
				Start: other.End,
				End:   tf.End,
			},
		}
	}
	if other.End.Before(tf.End) && !other.Start.After(tf.Start) {
		// It overlaps with leaving a gap at the end
		return []TimeFrame{
			{
				Start: other.End,
				End:   tf.End,
			},
		}
	}
	if other.Start.After(tf.Start) && !other.End.Before(tf.End) {
		// It overlaps with leaving a gap at the start
		return []TimeFrame{
			{
				Start: tf.Start,
				End:   other.End,
			},
		}
	}
	log.Fatal("unreachable code")
	return []TimeFrame{}
}

func SubtractFromMany(tfs []TimeFrame, sub *TimeFrame) (result []TimeFrame) {
	for _, tf := range tfs {
		result = append(result, tf.Subtract(sub)...)
	}
	return
}

func (tf* TimeFrame) StartsWithMonth() bool {
  return time.Date(tf.Start.Year(), tf.Start.Month(), 1, 0, 0, 0, 0, time.UTC) == tf.Start
}

func (tf* TimeFrame) EndsWithMonth() bool {
  return time.Date(tf.Start.Year(), tf.Start.Month(), 1, 0, 0, 0, 0, time.UTC).AddDate(0, 1, 0) == tf.End
}

func (tf* TimeFrame) IsFullMonth() bool {
  return tf.StartsWithMonth() && tf.EndsWithMonth()
}
