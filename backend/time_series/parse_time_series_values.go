package time_series

import (
	"fmt"
	"strconv"
	"time"
)

var (
	// timeSeriesDateFormats are the expected date formats in time series data
	timeSeriesDateFormats = []string{
		"2006-01-02",
		"2006-01-02 15:04:05",
	}
)

// parseDate parses a date value from a string.
// An error is returned if the value is not in one of the dateFormat formats.
func parseDate(v string) (time.Time, error) {
	for _, format := range timeSeriesDateFormats {
		t, err := time.Parse(format, v)
		if err != nil {
			continue
		}
		return t, nil
	}
	return time.Time{}, fmt.Errorf("applicable date format not found for date %s", v)
}

// parseFloat parses a float value.
// An error is returned if the value is not a float value.
func parseFloat(val string) (float32, error) {
	res, err := strconv.ParseFloat(val, 32)
	return float32(res), err
}

// parseInt parses an int value.
// An error is returned if the value is not an int value.
func parseInt(val string) (int, error) {
	i, err := strconv.ParseInt(val, 10, 64)
	if err != nil {
		return 0, err
	}
	return int(i), nil
}
