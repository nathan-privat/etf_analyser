package main

import (
  "etf_analyzer_backend/candle_series"
  "etf_analyzer_backend/config"
  "etf_analyzer_backend/storage"
	"etf_analyzer_backend/time_series"
	"fmt"
	"log"
	"net/http"
  "os"
  "strings"
	"time"
)

var dataStorage = storage.Storage{
  DataBasePath: os.ExpandEnv("$HOME/.config/etf_analyser/"),
}

func handleData(w http.ResponseWriter, r *http.Request, pathParts []string) {
	if len(pathParts) != 3 {
		w.WriteHeader(401)
		fmt.Fprintf(w, "unexpected number of parameters, expected symbol from and till, got: %v", pathParts)
		return
	}
	// Check the data format
	symbol := pathParts[0]
	from, err := time.Parse("2006-01-02T15-04-05", pathParts[1])
	if err != nil {
		w.WriteHeader(401)
		fmt.Fprintf(w, "unexpected parameter from")
		return
	}
	till, err := time.Parse("2006-01-02T15-04-05", pathParts[2])
	if err != nil {
		w.WriteHeader(401)
		fmt.Fprintf(w, "unexpected parameter from")
		return
	}
	// Check if our datasets are available
	if dataStorage.IsAvailable(symbol, time_series.TimeFrame{from, till}) {
		data, err := dataStorage.GetTimeSeries(symbol, time_series.TimeFrame{from, till})
		if err != nil {
			w.WriteHeader(503)
			fmt.Fprintf(w, "error getting timeseries data: %s", err.Error())
			return
		}
		w.WriteHeader(200)
		candle_series.WriteTimeSeriesCsvData(w, data)
	} else {
		dataStorage.QueryDownload(storage.DownloadQuery{
			symbol,
			time_series.TimeFrame{
				from,
				till,
			},
		},
		)
		w.WriteHeader(201)
		return
	}
}

func handler(w http.ResponseWriter, r *http.Request) {

	pathParts := strings.Split(r.URL.Path, "/")
	if len(pathParts) > 0 && pathParts[0] == "" {
	  pathParts = pathParts[1:]
  }

	switch pathParts[0] {
	case "data":
		handleData(w, r, pathParts[1:])
		return
	}

	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}

func main() {
  c := config.ReadConfig(os.ExpandEnv("$HOME/.config/etf_analyser/config.conf"))

	dataStorage.StartDownloader(c.ApiKey)
	http.HandleFunc("/", handler)
	log.Fatalf("Error on servie: %v", http.ListenAndServe(":9876", nil))
}
