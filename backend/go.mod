module etf_analyzer_backend

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/Finnhub-Stock-API/finnhub-go v1.2.1
	github.com/RudolfVonKrugstein/go-alpha-vantage v0.0.0-20200902113537-458a0120860b
	github.com/golang/protobuf v1.4.2
	github.com/pkg/errors v0.9.1
	google.golang.org/protobuf v1.25.0
)
