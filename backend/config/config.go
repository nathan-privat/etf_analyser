package config

import (
  "github.com/BurntSushi/toml"
  "log"
  "os"
)

// Info from config file
type Config struct {
  ApiKey   string
}

// Reads info from config file
func ReadConfig(configfile string) Config {
  _, err := os.Stat(configfile)
  if err != nil {
    log.Fatal("Config file is missing: ", configfile)
  }

  var config Config
  if _, err := toml.DecodeFile(configfile, &config); err != nil {
    log.Fatal(err)
  }
  //log.Print(config.Index)
  return config
}
