package candle_series

import (
  "errors"
  protobuf "etf_analyzer_backend/protobuf"
  "etf_analyzer_backend/time_series"
)

type CandleSeries protobuf.CandleSeries

func (ts* CandleSeries) IsIntervalSynced(other* CandleSeries) bool {
  return ts.Interval == other.Interval && int32(ts.TimeFrame.Start.Unix() - other.TimeFrame.Start.Unix()) % ts.Interval == 0
}

func (ts *TimeSeries) Trim(tf *time_series.TimeFrame) *TimeSeries {
  if ts.TimeFrame.Within(tf) {
    return ts
  }
  newTimeFrame := ts.TimeFrame.Intersection(tf)
  if newTimeFrame == nil {
    return nil
  }
  var newStartIndex, newEndIndex = 0, len(ts.Values)
  for i, v := range ts.Values {
    if v.Timestamp < tf.Start.Unix() {
      newStartIndex = i
    }
    if v.Timestamp > tf.End.Unix() {
      newEndIndex = i
    }
  }
  return &TimeSeries{
    Values: ts.Values[newStartIndex:newEndIndex],
    TimeFrame: *newTimeFrame,
    Interval: ts.Interval,
  }
}

func Union(a *TimeSeries, b *TimeSeries) (result *TimeSeries, err error) {
  // Test if the time series are compatible
  if !a.IsIntervalSynced(b) {
    err = errors.New("time series are not interval-synced")
    return
  }

  totalTimeFrame, err := a.TimeFrame.UnionIfOverlap(&b.TimeFrame)
  if err != nil {
    return
  }
  first, second := a, b
  // Ensure forst starts before second
  if second.TimeFrame.Start.Before(first.TimeFrame.Start) {
    first, second = second, first
  }
  // If second is not longer than first, its just first
  if !second.TimeFrame.End.After(first.TimeFrame.End) {
    result = first
    return
  }
  // OK, we have to take make a new one ...
  result = &TimeSeries{
    Values: append(first.Values, second.Trim(&time_series.TimeFrame{first.TimeFrame.End, second.TimeFrame.End}).Values...),
    Interval: first.Interval,
    TimeFrame: *totalTimeFrame,
  }
  return
}
