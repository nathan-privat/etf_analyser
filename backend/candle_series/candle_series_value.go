package candle_series

import (
  protobuf "etf_analyzer_backend/protobuf"
  "time"
)

// CandleSeriesValue is a piece of data for a given time about stock prices
type CandleSeriesValue = protobuf.CandleSeriesValue

// sortTimeSeriesValuesByDate allows TimeSeriesValue
// slices to be sorted by date in ascending order
type sortCandleSeriesValuesByDate []*CandleSeriesValue

func (b sortCandleSeriesValuesByDate) Len() int      { return len(b) }
func (b sortCandleSeriesValuesByDate) Less(i, j int) bool { return b[i].GetTimestamp() < b[j].GetTimestamp() }
func (b sortCandleSeriesValuesByDate) Swap(i, j int) { b[i], b[j] = b[j], b[i] }

func SetTimeStampWithTime(v *CandleSeriesValue, time time.Time) {
  v.Timestamp = time.Unix()
}

func SetOpenWithFloat(v *CandleSeriesValue, open float64) {
  v.Open = open
}

func SetCloseWithFloat(v *CandleSeriesValue, close float64) {
  v.Close = close
}

func SetHighWithFloat(v *CandleSeriesValue, high float64) {
  v.High = high
}

func SetLowWithFloat(v *CandleSeriesValue, low float64) {
  v.Low = low
}

func SetVolumeWithFloat(v *CandleSeriesValue, volume float64) {
  v.Volume = volume
}

func GetTimestamp(v *CandleSeriesValue) int64 {
  return v.Timestamp
}

func GetTimestampTime(v *CandleSeriesValue) time.Time {
  return time.Unix(v.Timestamp, 0)
}

func GetOpenFloat(v *CandleSeriesValue) float64 {
  return v.GetOpen()
}

func GetCloseFloat(v *CandleSeriesValue) float64 {
  return v.GetClose()
}

func GetHighFloat(v *CandleSeriesValue) float64 {
  return v.GetHigh()
}

func GetLowFloat(v *CandleSeriesValue) float64 {
  return v.GetLow()
}

func GetVolumeFloat(v *CandleSeriesValue) float64 {
  return v.GetVolume()
}
