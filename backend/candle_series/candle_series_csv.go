package candle_series

import (
  "encoding/csv"
  protobuf "etf_analyzer_backend/protobuf"
  "etf_analyzer_backend/time_series"
  "fmt"
  "github.com/pkg/errors"
  "io"
  "sort"
  "time"
  "unicode"
)

// parseTimeSeriesCsvData will parse csv data from a reader
func parseCandleSeriesCsvData(r io.Reader) (*protobuf.CandleSeries, error) {

  reader := csv.NewReader(r)
  reader.ReuseRecord = true // optimization
  reader.LazyQuotes = true
  reader.TrimLeadingSpace = true

  values := make([]*CandleSeriesValue, 0, 64)

  // strip header
  if firstRecord, err := reader.Read(); err != nil {
    if err == io.EOF {
      return nil, nil
    }
    return nil, err
  } else {
    if !isCsvTimeSeriesHeader(firstRecord) {
      value, err := parseCsvCandleSeriesRecord(firstRecord)
      if err != nil {
        return nil, err
      }
      values = append(values, value)
    }
  }

  for {
    record, err := reader.Read()
    if err != nil {
      if err == io.EOF {
        break
      }
      return nil, err
    }
    value, err := parseCsvCandleSeriesRecord(record)
    if err != nil {
      return nil, err
    }
    values = append(values, value)
  }

  // sort values by date
  sort.Sort(sortTimeSeriesValuesByDate(values))

  if len(values) == 0 {
    return nil, errors.New("no values found in csv")
  }
  return &protobuf.CandleSeries{
    Values: values,
    TimeFrame: time_series.TimeFrame{
      time.Unix(values[0].Timestamp, 0),
      time.Unix(values[len(values)-1].Timestamp, 0),
    },
    Interval: int32(values[1].Timestamp - values[0].Timestamp),
  }, nil

}

func isCsvTimeSeriesHeader(s []string) bool {
  // We have a very simple criteria for time series header: Its a header if any
  // column
  for _, column := range s {
    for _, rune := range []rune(column) {
      if unicode.IsLetter(rune) {
        return true
      }
    }
  }
  return false
}

// parseDigitalCurrencySeriesRecord will parse an individual csv record
func parseCsvCandleSeriesRecord(s []string) (*CandleSeriesValue, error) {
  // these are the expected columns in the csv record
  const (
    timestamp = iota
    open
    high
    low
    close
    volume
  )

  value := &CandleSeriesValue{}

  d, err := time_series.parseDate(s[timestamp])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing timestamp %s", s[timestamp])
  }
  value.Timestamp = d.Unix()

  f, err := time_series.parseFloat(s[open])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing open %s", s[open])
  }
  SetOpenWithFloat(value, f)

  f, err = time_series.parseFloat(s[high])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing high %s", s[high])
  }
  SetHighWithFloat(value, f)

  f, err = time_series.parseFloat(s[low])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing low %s", s[low])
  }
  SetLowWithFloat(value, f)

  f, err = time_series.parseFloat(s[close])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing close %s", s[close])
  }
  SetCloseWithFloat(value, f)

  f, err = time_series.parseFloat(s[volume])
  if err != nil {
    return nil, errors.Wrapf(err, "error parsing volume %s", s[volume])
  }
  SetVolumeWithFloat(value, f)

  return value, nil
}

// writeTimeSeriesProtoBufData will write protobuf data to a writer
func WriteTimeSeriesCsvData(w io.Writer, series *TimeSeries) error {
  for _, value := range series.Values {
    fmt.Fprintf(w, "%d,%f,%f,%f,%f,%f\n",
      GetTimestamp(value), GetOpenFloat(value), GetHighFloat(value), GetLowFloat(value), GetCloseFloat(value), GetVolumeFloat(value))
  }
  return nil
}
