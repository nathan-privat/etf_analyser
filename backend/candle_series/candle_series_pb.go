package candle_series

import (
  "etf_analyzer_backend/time_series"
  protobuf "etf_analyzer_backend/time_series/protobuf"
  "github.com/golang/protobuf/proto"
  "io"
  "io/ioutil"
  "os"
  "time"
)

// parseTimeSeriesProtoBufData will parse protobuf data from a reader
func parseCandleSeriesProtoBufData(r io.Reader) (*TimeSeries, error) {
  timeSeriesPb := &protobuf.TimeSeries{}

  b, err := ioutil.ReadAll(r)
  if err != nil {
    return nil, err
  }
  if err := proto.Unmarshal(b, timeSeriesPb); err != nil {
    return nil, err
  }
  return &TimeSeries{
    Values: timeSeriesPb.Values,
    TimeFrame: time_series.TimeFrame{
      Start: time.Unix(timeSeriesPb.TimeFrame.Start, 0),
      End: time.Unix(timeSeriesPb.TimeFrame.End, 0),
    },
    Interval: timeSeriesPb.Interval,
    Symbol: timeSeriesPb.Symbol,
  }, nil
}

// writeTimeSeriesProtoBufData will write protobuf data to a writer
func writeTimeSeriesProtoBufData(w io.Writer, timeSeries *TimeSeries) error {
  timeSeriesPb := &protobuf.TimeSeries{
    Interval: timeSeries.Interval,
    Values: timeSeries.Values,
    TimeFrame: &protobuf.TimeFrame{
      Start: timeSeries.TimeFrame.Start.Unix(),
      End:   timeSeries.TimeFrame.End.Unix(),
    },
    Symbol: timeSeries.Symbol,
  }

  if b, err := proto.Marshal(timeSeriesPb); err != nil {
    return err
  } else {
    if _, err := w.Write(b); err != nil {
      return err
    }
  }

  return nil
}

func (ts*time_series.TimeSeries) SaveTimeSeriesProtoBuf(fileName string) (err error) {
  f, err := os.OpenFile(fileName, os.O_CREATE | os.O_WRONLY, 0)
  if err != nil {
    return
  }
  defer f.Close()

  err = writeTimeSeriesProtoBufData(f, ts)
  return
}

func LoadTimeSeriesProtoBuf(fileName string) (*TimeSeries, error) {
  f, err := os.Open(fileName)
  if err != nil {
    return nil, err
  }
  defer f.Close()

  return parseTimeSeriesProtoBufData(f)
}
